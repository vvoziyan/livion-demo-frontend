describe("Signup", () => {
  it("All form elements are present", () => {
    // Navigate to signup page
    cy.visit("/signup");

    // Verify form inputs and buttons existance
    cy.get('[name="name"]').should("be.exist");
    cy.get('[name="surname"]').should("be.exist");
    cy.get('[name="email"]').should("be.exist");
    cy.get('[name="phone"]').should("be.exist");
    cy.get('[name="password"]').should("be.exist");
    cy.findByRole("button", {
      name: /register/i,
    }).should("be.exist");
    cy.findByRole("button", {
      name: /login/i,
    }).should("be.exist");
  });

  it("User can navigate to login page", () => {
    // Navigate to signup page
    cy.visit("/signup");

    // Login page button
    const loginPageButton = cy.findByRole("button", {
      name: /login/i,
    });

    // Click login page button
    loginPageButton.click();

    // Verify page url
    cy.url().should("contain", "/login");
  });

  it("User can't submit empty form", () => {
    // Navigate to signup page
    cy.visit("/signup");

    // Submit empty form
    cy.findByRole("button", {
      name: /register/i,
    }).click();

    // Verify form helper messages
    cy.findByText("Name required").should('be.exist')
    cy.findByText("Surname required").should('be.exist')
    cy.findByText("Phone number required").should('be.exist')
    cy.findByText("Email required").should('be.exist')
    cy.findByText("Password required").should('be.exist')

    // Verify page url
    cy.url().should("contain", "/signup");
  });
});
