import React, { FC, MouseEvent, useEffect } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { Stack, useTheme } from "@mui/material";
import { GET_USER_DATA } from "../../API/GraphQL/userData";
import { useQuery } from "@apollo/client";
import { useUser } from "../../Services/UserContex/userContext";
import { useNavigate } from "react-router-dom";

export const Header: FC = () => {
  const userContext = useUser();
  const theme = useTheme();

  const { data, loading, error } = useQuery(GET_USER_DATA, {
    variables: { userId: userContext?.activeUser?.id },
    context: {
      headers: {
        Authorization: userContext?.activeUser?.token,
      },
    },
    onError: (e) => console.log(">", e.cause ),

  });

  useEffect(() => console.log(data), [data]);
  useEffect(() => console.log(error), [error]);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenu = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => console.log(userContext?.user()), []);

  useEffect(() => {if(error?.graphQLErrors.map((err) => err.extensions.code).includes('UNAUTHENTICATED')) userContext?.logout()}, [error])

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          Livion
        </Typography>
        {userContext?.activeUser && data?.user ? (
          <Box sx={{ flexGrow: 0 }}>
            <Button
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
              endIcon={
                <Avatar
                  sx={{ bgColor: theme.palette.primary.main }}
                  alt="Remy Sharp"
                >
                  VV
                </Avatar>
              }
            >
              <Typography noWrap>
                {data?.user?.name} {data?.user?.surname}
              </Typography>
            </Button>
            <Menu
              id="user-menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem
                onClick={() => {
                  userContext.logout();
                  handleClose();
                }}
              >
                Logout
              </MenuItem>
            </Menu>
          </Box>
        ) : null}
      </Toolbar>
    </AppBar>
  );
};
