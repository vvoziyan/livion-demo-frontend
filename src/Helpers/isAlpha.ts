export const isAlpha = (value: string): boolean =>
  // eslint-disable-next-line no-useless-escape
  /^[a-zA-Z]+$/.test(
    value
  );
