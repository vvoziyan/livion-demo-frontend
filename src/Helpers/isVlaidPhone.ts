export const isValidPhone = (phone: string): boolean =>
  // eslint-disable-next-line no-useless-escape
  /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(
    phone
  );
