import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Box from "@mui/material/Box";
import { FC, useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { Alert, Backdrop, CircularProgress, Divider, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../Services/UserContex/userContext";
import { isAlpha } from "../../Helpers/isAlpha";
import { isValidPhone } from "../../Helpers/isVlaidPhone";
import { isValidEmail } from "../../Helpers/isValidEmail";

type SignupInputs = {
  name: string;
  surname: string;
  phone: string;
  email: string;
  password: string;
};

export const SignupPage: FC = () => {
  const userContext = useUser();
  const navigator = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<SignupInputs>();

  const [isSignupSuccessfull, setIsSignupSuccessfull] = useState<boolean>(true);
  const [signupRequestErrorMessage, setSignupRequestErrorMessage] = useState<string>("Something went wrong");

  const [loading, setLoading] = useState<boolean>(false); 

  const onSubmit: SubmitHandler<SignupInputs> = async (data) => {
    setLoading(true);
    const signup = await userContext?.signup({
      name: data.name,
      surname: data.surname,
      phone: data.phone,
      email: data.email,
      password: data.password
    },
    true
    );

    console.log(signup)

    if (signup?.id && signup.token) {
      setIsSignupSuccessfull(true);
    } else if (signup && signup.errorMessage) {
      setIsSignupSuccessfull(false);
      setSignupRequestErrorMessage(signup.errorMessage);
    }
    else {
      setIsSignupSuccessfull(false);
      setSignupRequestErrorMessage("Something went wrong")
    }
    setLoading(false);
  };

  
  
  useEffect(() => {
    if (userContext?.activeUser) {
      navigator("/");
    }
  }, [userContext]);

  return (
    <>
    <Box
      sx={{
        marginTop: 8,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        p: 2,
      }}
    >
      <Box sx={{ maxWidth: 400 }}>
        <Box sx={{ px: 10, pb: 2 }}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 80 15"
            fill="#ccc"
          >
            <path
              d="M55 0c-.97 0-1.881.2-2.73.596a7.109 7.109 0 00-2.22 1.61A7.725 7.725 0 0048 7.502a7.725 7.725 0 002.05 5.297 7.108 7.108 0 002.22 1.61c.849.396 1.76.591 2.73.591s1.886-.195 2.735-.59a7.108 7.108 0 002.22-1.611A7.675 7.675 0 0062 7.502a7.678 7.678 0 00-2.045-5.296 7.11 7.11 0 00-2.22-1.61A6.416 6.416 0 0055 0zm0 2.267c.664 0 1.29.138 1.885.414a4.929 4.929 0 011.55 1.116 5.31 5.31 0 011.04 1.666c.259.641.39 1.32.39 2.04a5.36 5.36 0 01-.39 2.044 5.508 5.508 0 01-1.045 1.671 4.727 4.727 0 01-1.545 1.116c-.594.27-1.22.404-1.885.404a4.465 4.465 0 01-1.89-.404 4.866 4.866 0 01-1.55-1.116 5.413 5.413 0 01-1.05-1.67 5.362 5.362 0 01-.39-2.046c0-.719.131-1.398.39-2.04a5.255 5.255 0 011.05-1.665 5.055 5.055 0 011.55-1.116A4.386 4.386 0 0155 2.267zM24.929 0c-.662.333-1.36.683-1.929.977 2.098 4.91 4.308 9.795 6.096 14.023h1.808C32.987 10.106 35.222 5.17 37 .977 36.339.64 35.645.285 35.071 0c-1.686 3.893-3.375 7.783-5.063 11.675C28.283 7.798 26.672 3.869 24.928 0zm48.005 0a6.317 6.317 0 00-2.68.604 7.105 7.105 0 00-2.222 1.605 7.65 7.65 0 00-1.484 2.375A7.775 7.775 0 0066 7.505V15h2.138V7.505c0-.729.133-1.4.39-2.032a5.401 5.401 0 011.042-1.664c.44-.476.938-.854 1.521-1.129a4.316 4.316 0 011.843-.406h.137a4.313 4.313 0 011.838.406 4.904 4.904 0 011.521 1.13 5.415 5.415 0 011.432 3.696V15H80V7.505a7.77 7.77 0 00-.542-2.92 7.66 7.66 0 00-1.49-2.376A7.105 7.105 0 0075.746.604 6.318 6.318 0 0073.07 0h-.137zM0 0v7.495c0 1.035.178 2.012.533 2.92a7.656 7.656 0 001.445 2.37 6.94 6.94 0 002.162 1.61c.81.391 1.68.6 2.608.605H11v-2.274H6.748a4.105 4.105 0 01-1.794-.406 4.784 4.784 0 01-1.48-1.129 5.464 5.464 0 01-1.02-1.669 5.448 5.448 0 01-.374-2.027V0H0zm16 15h2V0h-2zm25 0h2V0h-2z"
              fill-rule="evenodd"
            />
          </svg>
        </Box>
        <Box
          component="form"
          onSubmit={() => console.log("Submit")}
          sx={{ mt: 1 }}
        >
          {!isSignupSuccessfull ? (
              <Alert variant="outlined" severity="error">
                {signupRequestErrorMessage}
              </Alert>
            ) : null}
          <TextField
            error={errors.name ? true : false}
            helperText={errors.name?.message}
            margin="normal"
            fullWidth
            label="Name"
            autoFocus
            {...register("name", {
              required: { value: true, message: "Name required" },
              validate: {
                isAlpha: (value) => isAlpha(value) || "Only letters allowed"
              }
            })}
          />
          <TextField
            error={errors.surname ? true : false}
            helperText={errors.surname?.message}
            margin="normal"
            fullWidth
            label="Surname"
            {...register("surname", {
              required: { value: true, message: "Surname required" },
              validate: {
                isAlpha: (value) => isAlpha(value) || "Only letters allowed"
              }
            })}
          />
          <TextField
            error={errors.phone ? true : false}
            helperText={errors.phone?.message}
            margin="normal"
            fullWidth
            label="Phone number"
            {...register("phone", {
              required: { value: true, message: "Phone number required" },
              validate: {
                isPhone: (value) => isValidPhone(value) || "Should be phone number"
              }
            })}
          />
          <TextField
            error={errors.email ? true : false}
            helperText={errors.email?.message}
            margin="normal"
            fullWidth
            label="Email Address"
            {...register("email", {
              required: { value: true, message: "Email required" },
              validate: {
                isEmail: (value) => isValidEmail(value) || "Should be email"
              }
            })}
          />
          <TextField
            error={errors.password ? true : false}
            helperText={errors.password?.message}
            margin="normal"
            fullWidth
            label="Password"
            type="password"
            {...register("password", {
              required: { value: true, message: "Password required" },
            })}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            onClick={handleSubmit(onSubmit)}
          >
            Register
          </Button>
          <Divider>Or</Divider>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 2, mb: 2 }}
            onClick={() => navigator("/login")}
          >
            Login
          </Button>
          <Typography
            variant="body2"
            color="text.primary"
            align="center"
            marginTop={4}
          >
            {"v1.0.43 Copyright © "}
            <Link color="inherit" href="https://livion.fi/">
              Livion Oy
            </Link>{" "}
            {new Date().getFullYear()}
            {"."}
          </Typography>
        </Box>
      </Box>
    </Box>
    <Backdrop
    sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
    open={loading}
  >
    <CircularProgress color="inherit" />
  </Backdrop>
  </>
  );
};
