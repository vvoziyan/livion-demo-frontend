import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { FC, useEffect, useState, useRef } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import {
  Backdrop,
  CircularProgress,
  IconButton,
  Input,
  Stack,
  TablePagination,
  useTheme,
} from "@mui/material";
import Typography from "@mui/material/Typography";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import SearchIcon from "@mui/icons-material/Search";
import CloseIcon from "@mui/icons-material/Close";
import { CreateUserDialog } from "./Modules/CreateUserDialog/CreateUserDialog";
import { UpdateUserDialog } from "./Modules/UpdateUserDialog/UpdateUserDialog";
import {
  GET_USERS_LIST,
  GET_USERS_LIST_RETURN_TYPE,
  GET_USERS_LIST_VARIABLES,
} from "../../API/GraphQL/usersList";
import { useNavigate } from "react-router-dom";
import { useLazyQuery } from "@apollo/client";
import { useUser } from "../../Services/UserContex/userContext";

const limit = 5;

export const TablePage: FC = () => {
  const userContext = useUser();
  const navigator = useNavigate();

  const theme = useTheme();

  const [searchValue, setSearchValue] = useState<string>("");
  const [searchBarValue, setSearchBarValue] = useState<string>("");
  const searchBarValueRef = useRef(searchBarValue);
  searchBarValueRef.current = searchBarValue;

  const [tablePage, setTablePage] = useState<number>(0);

  const [createUserModalState, setCreateUserModalState] =
    useState<boolean>(false);

  const [updateUserModalState, setUpdateUserModalState] =
    useState<boolean>(false);
  const [updateUserId, setUpdateUserId] = useState<string | null>(null);

  const [getUsersData, { data, loading, error, refetch }] = useLazyQuery<
    GET_USERS_LIST_RETURN_TYPE,
    GET_USERS_LIST_VARIABLES
  >(GET_USERS_LIST, {
    variables: {
      limit: limit,
      pagination: limit * tablePage,
      search: searchValue,
    },
    context: {
      headers: {
        Authorization: userContext?.user()?.token,
      },
    },
    fetchPolicy: "no-cache",
  });

  // Updates searchValue, which leads to table refetch,
  // when user not write anything in search input for 500ms.
  // It done to reduce amount of requests.
  useEffect(() => {
    if (userContext?.activeUser) {
      setTimeout(() => {
        if (searchBarValue === searchBarValueRef.current) {
          setSearchValue(searchBarValueRef.current);
          setTablePage(0);
        }
      }, 500);
    }
  }, [searchBarValue]);

  // Get table data, if user logged in
  useEffect(() => {
    if (userContext?.activeUser) {
      getUsersData();
    }
  }, []);

  // Redirect to login page,
  // in case of logout
  useEffect(() => {
    if (!userContext?.activeUser) {
      navigator("/login");
    }
  }, [userContext]);

  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ maxWidth: 1000, width: "100%" }}>
          {/* Create user button */}
          <Grid container justifyContent="flex-end" mb={2}>
            <Button
              onClick={() => setCreateUserModalState(true)}
              variant="outlined"
            >
              + Create
            </Button>
          </Grid>

          <Paper>
            {/* Table top bar */}
            <Grid
              container
              alignItems="center"
              justifyContent="space-between"
              flexWrap={"nowrap"}
              sx={{
                backgroundColor: theme.palette.primary.light,
                py: 1,
                px: 2,
                borderTopLeftRadius: theme.shape.borderRadius,
                borderTopRightRadius: theme.shape.borderRadius,
              }}
            >
              <Typography
                variant="body1"
                color="primary.dark"
                component="div"
                sx={{ flexGrow: 1 }}
              >
                Users
              </Typography>
              <Input
                value={searchBarValue}
                onChange={(e) => setSearchBarValue(e.target.value)}
                disableUnderline
                placeholder="Search"
                sx={{
                  ml: 3,
                  border: `2px solid ${theme.palette.primary.main}`,
                  backgroundColor: theme.palette.background.default,
                  pl: 1,
                  borderRadius: theme.shape.borderRadius,
                  maxWidth: 450,
                  width: searchBarValue ? "100%" : 150,
                  transition: "width 0.3s linear",
                }}
                endAdornment={
                  searchValue ? (
                    <IconButton
                      size="small"
                      onClick={() => setSearchBarValue("")}
                    >
                      <CloseIcon fontSize="small" />
                    </IconButton>
                  ) : null
                }
                startAdornment={
                  <SearchIcon sx={{ mr: 1, color: theme.palette.grey[600] }} />
                }
              />
            </Grid>

            {/* Table */}
            <TableContainer>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow sx={{ color: theme.palette.primary.main }}>
                    <TableCell sx={{ color: theme.palette.primary.main }}>
                      Name
                    </TableCell>
                    <TableCell sx={{ color: theme.palette.primary.main }}>
                      Email
                    </TableCell>
                    <TableCell sx={{ color: theme.palette.primary.main }}>
                      Phone
                    </TableCell>
                    <TableCell sx={{ color: theme.palette.primary.main }}>
                      Organization permissions
                    </TableCell>
                    <TableCell
                      sx={{ color: theme.palette.primary.main }}
                    ></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {!loading &&
                    data?.users.result.map((row) => (
                      <TableRow
                        key={row._id}
                        sx={{
                          "&:last-child td, &:last-child th": { border: 0 },
                        }}
                      >
                        <TableCell component="th" scope="row">
                          {row.name} {row.surname}
                        </TableCell>
                        <TableCell>{row.email}</TableCell>
                        <TableCell>{row.phone}</TableCell>
                        <TableCell>
                          <Stack direction="column">
                            {row.roles?.map((role) => (
                              <Typography variant="body2">{role}</Typography>
                            ))}
                          </Stack>
                        </TableCell>
                        <TableCell align="right">
                          <IconButton
                            onClick={() => {
                              setUpdateUserId(row._id ? row._id : "");
                              setUpdateUserModalState(true);
                            }}
                          >
                            <MoreVertIcon />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>

            {/* Table bottom controls */}
            {data?.users?.result && data.users.countAll > limit ? (
              <TablePagination
                rowsPerPageOptions={[]}
                component="div"
                count={data.users.countAll}
                rowsPerPage={limit}
                page={tablePage}
                onPageChange={(event: unknown, newPage: number) => {
                  console.log(
                    data.users.result.slice(
                      tablePage * limit,
                      tablePage * limit + limit
                    )
                  );
                  setTablePage(newPage);
                }}
              />
            ) : null}
          </Paper>
        </Box>
      </Box>

      <CreateUserDialog
        open={createUserModalState}
        handleClose={() => {
          setCreateUserModalState(false);
          refetch();
        }}
      />

      {updateUserId != null && updateUserModalState ? (
        <UpdateUserDialog
          userId={updateUserId}
          open={updateUserModalState}
          handleClose={() => {
            setUpdateUserModalState(false);
            refetch();
          }}
        />
      ) : null}

      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};
