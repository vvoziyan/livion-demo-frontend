import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { FC, useState } from "react";
import {
  Backdrop,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Grid,
  IconButton,
  InputAdornment
} from "@mui/material";
import { useMutation, useQuery } from "@apollo/client";
import {
  GET_USER_DATA,
  GET_USER_DATA_VARIABLES,
  GET_USER_DATA_RETURN_TYPE,
} from "../../../../API/GraphQL/userData";
import { SubmitHandler, useForm } from "react-hook-form";
import { isAlpha } from "../../../../Helpers/isAlpha";
import { isValidEmail } from "../../../../Helpers/isValidEmail";
import { isValidPhone } from "../../../../Helpers/isVlaidPhone";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import {
  UPDATE_USER_DATA,
  UPDATE_USER_DATA_VARIABLES,
  UPADE_USER_DATA_RETURN_TYPE,
} from "../../../../API/GraphQL/updateUserData";
import { useUser } from "../../../../Services/UserContex/userContext";

type UpdateUserInputs = {
  name: string;
  surname: string;
  phone: string;
  email: string;
  password: string;
};

export const UpdateUserDialog: FC<{
  open: boolean;
  handleClose: (...params: any[]) => any;
  userId: string;
}> = ({ open, handleClose, userId }) => {
  const userContext = useUser();

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
    reset,
  } = useForm<UpdateUserInputs>();

  const [showPassword, setShowPassword] = useState<boolean>(false);

  const { data, loading, error, refetch } = useQuery<
    GET_USER_DATA_RETURN_TYPE,
    GET_USER_DATA_VARIABLES
  >(GET_USER_DATA, {
    variables: { userId },
    context: {
      headers: {
        Authorization: userContext?.user()?.token,
      },
    },
    fetchPolicy: "no-cache",
  });

  const [updateUser, { ...mutation }] = useMutation<
    UPADE_USER_DATA_RETURN_TYPE,
    UPDATE_USER_DATA_VARIABLES
  >(UPDATE_USER_DATA, {
    context: {
      headers: {
        Authorization: userContext?.user()?.token,
      },
    },
    fetchPolicy: "no-cache",
  });

  const onSubmit: SubmitHandler<UpdateUserInputs> = async (data) => {
    const { password, ...updatedData } = data;

    const result = await updateUser({
      variables: {
        userId,
        updatedData: {
          ...updatedData,
          password: password != "" ? password : undefined,
        },
      },
    });
    console.log(result);

    reset();
    handleClose();
  };

  return loading ? (
    <Backdrop
      sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={loading}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  ) : (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>
        Update {data?.user?.name} {data?.user?.surname} profile
      </DialogTitle>
      <DialogContent>
        <DialogContentText>Create new user</DialogContentText>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <TextField
              autoFocus
              error={errors.name ? true : false}
              helperText={errors.name?.message}
              defaultValue={data?.user.name}
              margin="dense"
              label="Name"
              type="email"
              fullWidth
              variant="standard"
              {...register("name", {
                required: { value: true, message: "Name required" },
                validate: {
                  isAlpha: (value) => isAlpha(value) || "Only letters allowed",
                },
              })}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              error={errors.surname ? true : false}
              helperText={errors.surname?.message}
              defaultValue={data?.user.surname}
              margin="dense"
              label="Surname"
              type="text"
              fullWidth
              variant="standard"
              {...register("surname", {
                required: { value: true, message: "Surname required" },
                validate: {
                  isAlpha: (value) => isAlpha(value) || "Only letters allowed",
                },
              })}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              error={errors.email ? true : false}
              helperText={errors.email?.message}
              defaultValue={data?.user.email}
              margin="dense"
              label="Email"
              type="text"
              fullWidth
              variant="standard"
              {...register("email", {
                required: { value: true, message: "Email required" },
                validate: {
                  isEmail: (value) => isValidEmail(value) || "Should be email",
                },
              })}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              error={errors.phone ? true : false}
              helperText={errors.phone?.message}
              defaultValue={data?.user.phone}
              margin="dense"
              label="Phone"
              type="text"
              fullWidth
              variant="standard"
              {...register("phone", {
                required: { value: true, message: "Phone required" },
                validate: {
                  isPhone: (value) =>
                    isValidPhone(value) || "Should be phone number",
                },
              })}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              error={errors.password ? true : false}
              helperText={errors.password?.message}
              margin="dense"
              label="Password"
              type={showPassword ? "text" : "password"}
              fullWidth
              variant="standard"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword((state) => !state)}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              {...register("password")}
            />
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={2}>
              {["admin", "user", "worker", "owner"].map((key) => (
                <Grid item xs={3}>
                  <FormControlLabel
                    label={key}
                    control={<Checkbox checked={true} onChange={() => {}} />}
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSubmit(onSubmit)}>Update user</Button>
      </DialogActions>
    </Dialog>
  );
};
