import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { FC, useState } from "react";
import {
  Alert,
  Grid,
  IconButton,
  InputAdornment,
} from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { isValidEmail } from "../../../../Helpers/isValidEmail";
import { isValidPhone } from "../../../../Helpers/isVlaidPhone";
import { isAlpha } from "../../../../Helpers/isAlpha";
import { useUser } from "../../../../Services/UserContex/userContext";

type SignupInputs = {
  name: string;
  surname: string;
  phone: string;
  email: string;
  password: string;
};

export const CreateUserDialog: FC<{
  open: boolean;
  handleClose: (...params: any[]) => any;
}> = ({ open, handleClose }) => {
  const userContext = useUser();

  const [isSignupSucessfull, setIsSignupSuccessfull] = useState<boolean>(true);
  const [signupRequestErrorMessage, setSignupRequestErrorMessage] = useState<string>("Something went wrong");

  const [showPassword, setShowPassword] = useState<boolean>(false);

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
    reset,
  } = useForm<SignupInputs>();

  const onSubmit: SubmitHandler<SignupInputs> = async (data) => {
    const signup = await userContext?.signup({
      name: data.name,
      surname: data.surname,
      phone: data.phone,
      email: data.email,
      password: data.password
    },
    false
    );

    if (signup?.id && signup.token) {
      setIsSignupSuccessfull(true);
      closeDialog();
    } else if (signup && signup.errorMessage) {
      setIsSignupSuccessfull(false);
      setSignupRequestErrorMessage(signup.errorMessage);
    }
    else {
      setIsSignupSuccessfull(false);
      setSignupRequestErrorMessage("Something went wrong")
    }
  };

  const closeDialog = () => {
    reset({ email: "", name: "", password: "", phone: "", surname: "" });
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Create new user</DialogTitle>
      <DialogContent>
        <DialogContentText>Create new user</DialogContentText>
        {!isSignupSucessfull ? (
          <Alert variant="outlined" severity="error">
            {signupRequestErrorMessage}
          </Alert>
        ) : null}
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <TextField
              autoFocus
              error={errors.name ? true : false}
              helperText={errors.name?.message}
              margin="dense"
              label="Name"
              type="email"
              fullWidth
              variant="standard"
              {...register("name", {
                required: { value: true, message: "Name required" },
                validate: {
                  isAlpha: (value) => isAlpha(value) || "Only letters allowed"
                }
              })}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              error={errors.surname ? true : false}
              helperText={errors.surname?.message}
              margin="dense"
              label="Surname"
              type="text"
              fullWidth
              variant="standard"
              {...register("surname", {
                required: { value: true, message: "Surname required" },
                validate: {
                  isAlpha: (value) => isAlpha(value) || "Only letters allowed"
                }
              })}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              error={errors.email ? true : false}
              helperText={errors.email?.message}
              margin="dense"
              label="Email"
              type="text"
              fullWidth
              variant="standard"
              {...register("email", {
                required: { value: true, message: "Email required" },
                validate: {
                  isEmail: (value) => isValidEmail(value) || "Should be email"
                }
              })}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              error={errors.phone ? true : false}
              helperText={errors.phone?.message}
              margin="dense"
              label="Phone"
              type="text"
              fullWidth
              variant="standard"
              {...register("phone", {
                required: { value: true, message: "Phone required" },
                validate: {
                  isPhone: (value) => isValidPhone(value) || "Should be phone number"
                }
              })}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              error={errors.password ? true : false}
              helperText={errors.password?.message}
              margin="dense"
              label="Password"
              type={showPassword ? "text" : "password"}
              fullWidth
              variant="standard"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword((state) => !state)}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              {...register("password", {
                required: { value: true, message: "Password required" },
              })}
            />
          </Grid>

        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog}>Cancel</Button>
        <Button onClick={handleSubmit(onSubmit)}>Create user</Button>
      </DialogActions>
    </Dialog>
  );
};
