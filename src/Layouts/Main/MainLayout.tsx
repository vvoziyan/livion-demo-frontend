import React, { FC } from "react";
import { Header } from "../../Modules/Header/Header";
import logo from "./logo.svg";

export const MainLayout: FC = ({ children }) => {
  return(
      <>
      <Header />
      {children}
      </>
  );
};
