import { BrowserRouter, Route, Routes } from "react-router-dom";
import { MainLayout } from "./Layouts/Main/MainLayout";
import { LoginPage } from "./Pages/Login/LoginPage";
import { SignupPage } from "./Pages/Signup/SignupPage";
import { TablePage } from "./Pages/Table/TablePage";

function App() {

  return (
    <MainLayout>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<TablePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/signup" element={<SignupPage />} />
        </Routes>
      </BrowserRouter>
    </MainLayout>
  );
}

export default App;
