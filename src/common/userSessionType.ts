export type UserSessionType = {
    id: string;
    token: string;
}