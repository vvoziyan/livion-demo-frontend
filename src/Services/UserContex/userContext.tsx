import { createContext, FC, useContext, useState } from "react";
import { RESTApiEndpoints } from "../../API/Rest/endpoints";
import { CreateUserDTO, LoginDTO } from "../../API/Rest/types";
import { UserSessionType } from "../../common/userSessionType";

const user = (): UserSessionType | null => {
  const localStorageRecord = localStorage.getItem("current_user");
  if (!localStorageRecord) return null;

  let result = null;

  try {
    const session = JSON.parse(localStorageRecord);
    if (session?.id && session?.token) {
      result = { token: session.token, id: session.id };
    }
  } finally {
    return result;
  }
};

type UserContextPrototype = {
  login: (
    data: LoginDTO
  ) => Promise<Partial<UserSessionType & { errorMessage: string }>>;
  signup: (
    data: CreateUserDTO,
    remember: boolean
  ) => Promise<Partial<UserSessionType & { errorMessage: string }>>;
  logout: () => Promise<null | { errorMessage: string }>;
  user: () => UserSessionType | null;
  activeUser: UserSessionType | null;
};

export const UserContext = createContext<UserContextPrototype | null>(null);
export function useUser(): UserContextPrototype | null {
  return useContext(UserContext);
}

export const UserProvider: FC = (props) => {
  const [activeUser, setActiveUser] = useState<UserSessionType | null>(user());

  const signup = async (
    data: CreateUserDTO,
    remember: boolean
  ): Promise<Partial<UserSessionType & { errorMessage: string }>> => {
    const signupRequest = await RESTApiEndpoints.auth
      .signup(data)
      .then((result) =>
        result.status == 201 && result.data.userId && result.data.token
          ? { id: result.data.userId, token: result.data.token }
          : { errorMessage: result.data.message || "Something went wrong" }
      );

    if (signupRequest.id && signupRequest.token) {
      const session = { id: signupRequest.id, token: signupRequest.token };
      if (remember) {
        setActiveUser(session);
        localStorage.setItem("current_user", JSON.stringify(session));
      }
      return session;
    }

    return { errorMessage: signupRequest.errorMessage };
  };

  const login = async (
    data: LoginDTO
  ): Promise<Partial<UserSessionType & { errorMessage: string }>> => {
    const loginRequest = await RESTApiEndpoints.auth
      .login(data)
      .then((result) =>
        result.status == 201 && result.data.userId && result.data.token
          ? { id: result.data.userId, token: result.data.token }
          : { errorMessage: result.data.message || "Something went wrong" }
      );

    if (loginRequest.id && loginRequest.token) {
      const session = { id: loginRequest.id, token: loginRequest.token };
      setActiveUser(session);
      localStorage.setItem("current_user", JSON.stringify(session));
      return session;
    }

    return { errorMessage: loginRequest.errorMessage };
  };

  const logout = async (): Promise<{ errorMessage: string } | null> => {
    const active_user = user();
    if (!active_user) return null;

    const logoutRequest = await RESTApiEndpoints.auth
      .logout(active_user.token)
      .then((result) =>
        result.status == 200
          ? null
          : { errorMessage: result.data.message || "Something went wrong" }
      )
      .catch((err) => null);

    localStorage.removeItem("current_user");
    setActiveUser(null);

    if (!logoutRequest?.errorMessage) {
      return null;
    }

    return { errorMessage: logoutRequest.errorMessage };
  };

  return (
    <UserContext.Provider
      value={{
        login,
        signup,
        logout,
        user,
        activeUser,
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};
