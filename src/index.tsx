import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import React from "react";
import ReactDOM from "react-dom";
import { baseUrl } from "./API/constants";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { UserProvider } from "./Services/UserContex/userContext";

const theme = createTheme({
  palette: {
    primary: {main: "#467E8C", light: "#ECF2F5"}
  }
});

const client = new ApolloClient({
  uri: baseUrl + "/graphql",
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <React.StrictMode>
      <UserProvider>
      <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
      </ApolloProvider>
    </UserProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
