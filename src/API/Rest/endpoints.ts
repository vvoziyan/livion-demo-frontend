import axios from "axios";
import { baseUrl } from "../constants";
import { CreateUserDTO, LoginDTO } from "./types";

const http = axios.create({
  baseURL: `${baseUrl}`,
  headers: {
    "Content-type": "application/json",
  },
  validateStatus: (status) => true,
});

export const RESTApiEndpoints = {
  auth: {
    login: (loginData: LoginDTO) =>
      http.post<{ token?: string; userId?: string; message?: string }>(
        "/auth/login",
        loginData,
        { validateStatus: (status) => true }
      ),
    logout: (token: string) =>
      http.get<{ message?: string }>("/auth/logout", {
        headers: { authorization: token },
      }),
    signup: (signupData: CreateUserDTO) =>
      http.post<{ token?: string; userId?: string; message?: string }>(
        "/users/signup",
        signupData
      ),
  },
};
