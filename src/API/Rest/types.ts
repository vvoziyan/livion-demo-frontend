export type CreateUserDTO = {
  name: string;
  surname: string;
  phone: string;
  email: string;
  password: string;
};

export type LoginDTO = {
  email: string;
  password: string;
};
