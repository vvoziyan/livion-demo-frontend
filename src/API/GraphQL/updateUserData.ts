import { gql } from "@apollo/client";

export const UPDATE_USER_DATA = gql`
  mutation ($userId: String!, $updatedData: UpdateUserData!) {
    updateUser(userId: $userId, updatedData: $updatedData) {
      phone
      name
      surname
      email
    }
  }
`;

export type UPADE_USER_DATA_RETURN_TYPE = {
  updateUser: {
    phone: string;
    name: string;
    surname: string;
    email: string;
  };
};

export type UPDATE_USER_DATA_VARIABLES = {
  userId: string;
  updatedData: {
    email?: string;
    password?: string;
    phone?: string;
    name?: string;
    surname?: string;
  };
};
