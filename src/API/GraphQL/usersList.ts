import { gql } from "@apollo/client";

export const GET_USERS_LIST = gql`
  query ($limit: Int, $pagination: Int, $search: String) {
    users(limit: $limit, pagination: $pagination, search: $search) {
      limit
      pagination
      countAll
      result {
        _id
        name
        surname
        email
        phone
        roles
      }
    }
  }
`;

export type GET_USERS_LIST_RETURN_TYPE = {
  users: {
    limit: number;
    countAll: number;
    pagination: number;
    result: {
      _id: string;
      phone: string;
      name: string;
      surname: string;
      email: string;
      roles: string[];
    }[];
  };
};

export type GET_USERS_LIST_VARIABLES = {
  limit?: number;
  pagination?: number;
  search?: string;
};
