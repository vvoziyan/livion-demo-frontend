import { gql } from "@apollo/client";

export const GET_USER_DATA = gql`
  query ($userId: String!) {
    user(id: $userId) {
      name
      surname
      email
      phone
      roles
    }
  }
`;

export type GET_USER_DATA_RETURN_TYPE = {
  user: {
    phone: string;
    name: string;
    surname: string;
    email: string;
  };
};

export type GET_USER_DATA_VARIABLES = {
  userId: string;
};